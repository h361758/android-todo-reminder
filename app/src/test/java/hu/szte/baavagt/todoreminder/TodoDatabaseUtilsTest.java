package hu.szte.baavagt.todoreminder;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Calendar;

import hu.szte.baavagt.todoreminder.utils.TodoDatabaseUtils;

/**
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class TodoDatabaseUtilsTest {

  @Test
  public void testCreateValuesFromTodo() {
    Calendar calendar = TodoDatabaseUtils.convertStringToCalendar("2017-01-02 10:31");
    Assert.assertEquals(calendar.get(Calendar.YEAR), 2017);
    Assert.assertEquals(calendar.get(Calendar.MONTH), 0);
    Assert.assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 2);
    Assert.assertEquals(calendar.get(Calendar.HOUR_OF_DAY), 10);
    Assert.assertEquals(calendar.get(Calendar.MINUTE), 31);
  }

}
