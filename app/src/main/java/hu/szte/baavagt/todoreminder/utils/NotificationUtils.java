package hu.szte.baavagt.todoreminder.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.media.MediaPlayer;
import android.support.v4.app.NotificationCompat;

import hu.szte.baavagt.todoreminder.R;
import hu.szte.baavagt.todoreminder.model.Todo;

/**
 * Utility class for throwing notifications.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class NotificationUtils {

  private NotificationUtils() {

  }

  /**
   * Throws a notification for a given todo.
   *
   * @param context The application's context.
   * @param todo    The todo as the data.
   */
  public static void throwNotificationForTodo(Context context, Todo todo) {
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
      .setSmallIcon(todo.getCategory().getDrawable())
      .setContentTitle(todo.getTitle())
      .setContentText(todo.getSummary());

    NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    manager.notify(todo.getId(), notificationBuilder.build());

    playNotificationSound(context);
  }

  public static void playNotificationSound(Context context) {
    MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.notification);
    mediaPlayer.start();
  }

}
