package hu.szte.baavagt.todoreminder.database;

import android.provider.BaseColumns;

/**
 * Contract class for the application.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class TodoReminderContract {

  private TodoReminderContract() {

  }

  /**
   * Column definitions for the {@link hu.szte.baavagt.todoreminder.model.Todo} table.
   */
  public static class TodoEntry implements BaseColumns {
    public static final String TABLE_NAME = "todo";

    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_COLOR = "color";
    public static final String COLUMN_DATE_TIME = "date_time";
    public static final String COLUMN_SUMMARY = "summary";
  }

}
