package hu.szte.baavagt.todoreminder.listener;

import android.app.DialogFragment;
import android.util.Log;
import android.view.View;

import java.util.concurrent.Callable;

import hu.szte.baavagt.todoreminder.model.Todo;
import hu.szte.baavagt.todoreminder.model.enums.TodoColor;

/**
 * OnClick listener for the {@link hu.szte.baavagt.todoreminder.dialog.ColorPickerDialogFragment}.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class ColorChangeOnClickListener implements View.OnClickListener {

  private Todo todo;
  private TodoColor color;
  private DialogFragment dialog;
  private Callable<Void> callback;

  public ColorChangeOnClickListener(Todo todo, TodoColor color, DialogFragment dialog, Callable<Void> callback) {
    this.todo = todo;
    this.color = color;
    this.dialog = dialog;
    this.callback = callback;
  }

  /**
   * When a color is picked, the {@link Todo} object is updated and the dialog is dismissed. If a callback was
   * specified, it is also called.
   *
   * @param view The color icon that was clicked on.
   */
  @Override
  public void onClick(View view) {
    todo.setColor(color);

    if (callback != null) {
      try {
        callback.call();
      } catch (Exception e) {
        Log.e("ColorChangeListener", "Failed to call color change callback!", e);
      }
    }

    dialog.dismiss();
  }

}
