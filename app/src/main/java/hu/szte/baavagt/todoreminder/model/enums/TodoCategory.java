package hu.szte.baavagt.todoreminder.model.enums;

import hu.szte.baavagt.todoreminder.R;

/**
 * Specifies the categories for the {@link hu.szte.baavagt.todoreminder.model.Todo}s. Also holds references for the
 * labels and the drawable icons for convenience.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public enum TodoCategory {

  NONE(R.string.category_none, R.drawable.ic_category_none),
  ANNIVERSARY(R.string.category_anniversary, R.drawable.ic_category_anniversary),
  CALL(R.string.category_call, R.drawable.ic_category_call),
  DATE(R.string.category_date, R.drawable.ic_category_date),
  MEETING(R.string.category_meeting, R.drawable.ic_category_meeting),
  SCHOOL(R.string.category_school, R.drawable.ic_category_school),
  SHOPPING(R.string.category_shopping, R.drawable.ic_category_shopping),
  SPORT(R.string.category_sport, R.drawable.ic_category_sport),
  TRAVEL(R.string.category_travel, R.drawable.ic_category_travel);

  private int label;
  private int drawable;

  TodoCategory(int label, int drawable) {
    this.label = label;
    this.drawable = drawable;
  }

  public int getLabel() {
    return label;
  }

  public int getDrawable() {
    return drawable;
  }

}
