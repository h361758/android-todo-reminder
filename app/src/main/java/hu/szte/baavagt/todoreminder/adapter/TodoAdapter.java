package hu.szte.baavagt.todoreminder.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import hu.szte.baavagt.todoreminder.R;
import hu.szte.baavagt.todoreminder.model.Todo;

/**
 * Adapter for {@link Todo}s for a {@link android.widget.ListView}.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class TodoAdapter extends ArrayAdapter<Todo> {

  private static final SimpleDateFormat TODO_LIST_DATE_FORMAT = new SimpleDateFormat("yyyy. MM. dd. HH:mm", Locale.UK);

  /**
   * Using the ViewHolder pattern.
   */
  private static class ViewHolder {
    private LinearLayout color;
    private ImageView categoryIcon;
    private TextView title;
    private TextView dateTime;
    private TextView summary;
  }

  public TodoAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull Todo[] objects) {
    super(context, resource, objects);
  }

  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    ViewHolder viewHolder;
    if (convertView == null) {
      convertView = inflater.inflate(R.layout.list_todo_item, null);
      viewHolder = new ViewHolder();
      viewHolder.color = (LinearLayout) convertView.findViewById(R.id.list_todo_color_bar);
      viewHolder.categoryIcon = (ImageView) convertView.findViewById(R.id.list_todo_category_icon);
      viewHolder.title = (TextView) convertView.findViewById(R.id.list_todo_title);
      viewHolder.dateTime = (TextView) convertView.findViewById(R.id.list_todo_datetime);
      viewHolder.summary = (TextView) convertView.findViewById(R.id.list_todo_summary);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }

    Todo todo = getItem(position);

    // Fill data
    viewHolder.color.setBackgroundResource(todo.getColor().getDrawable());
    viewHolder.categoryIcon.setImageResource(todo.getCategory().getDrawable());
    viewHolder.title.setText(todo.getTitle());
    viewHolder.summary.setText(todo.getSummary());
    viewHolder.dateTime.setText(TODO_LIST_DATE_FORMAT.format(todo.getDateTime().getTime()));

    return convertView;
  }

}
