package hu.szte.baavagt.todoreminder.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.JsonWriter;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import hu.szte.baavagt.todoreminder.MainActivity;
import hu.szte.baavagt.todoreminder.R;
import hu.szte.baavagt.todoreminder.database.TodoOpenHelper;
import hu.szte.baavagt.todoreminder.model.Todo;

/**
 * {@link AsyncTask} for exporting the database into a simple json file.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class DatabaseExportAsyncTask extends AsyncTask<Context, Void, Boolean> {

  private static final SimpleDateFormat JSON_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.UK);
  private static final String SELECT_ALL_TODOS = "SELECT * FROM todo;";

  private static final String PREF_KEY = "hu.szte.baavagt.todoreminder";
  private static final String PREF_LAST_EXPORT = "lastExported";

  private Context context;

  /**
   * Handles the export process.
   *
   * @param params The application context. Only one parameter is expected, and that one is expected to be available.
   * @return True, if the database export was successful, false otherwise.
   */
  @Override
  protected Boolean doInBackground(Context... params) {
    if (params.length < 1) {
      Log.d("TodoDatabaseExport", "No context specified!");
      return false;
    }

    this.context = params[0];

    List<Todo> todos = queryAllTodos();

    try {
      File file = createFileForExport();
      FileOutputStream out = new FileOutputStream(file);
      writeTodoList(out, todos);
      out.close();
    } catch (IOException e) {
      Log.e("TodoDatabaseExport", "Failed to export database!", e);
      return false;
    }

    Log.d("TodoDatabaseExport", "Database exported.");
    return true;
  }

  /**
   * Creates the file for the database export. Also creates the parent directory if it doesn't exist yet.
   *
   * @return The file to export into.
   * @throws IOException if any error occurs during the creation of the file or the directories.
   */
  private File createFileForExport() throws IOException {
    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS + "/todoreminder/");
    if (!path.exists()) {
      path.mkdirs();
    }

    File file = new File(path, "todo-export.json");
    file.createNewFile();
    return file;
  }

  /**
   * Throws a toast message to notify the user of the result of  the export.
   *
   * @param successful The result of the export.
   */
  @Override
  protected void onPostExecute(Boolean successful) {
    if (successful) {
      SharedPreferences.Editor preferences = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE).edit();
      preferences.putString(PREF_LAST_EXPORT, JSON_DATE_FORMAT.format(Calendar.getInstance().getTime()));
      preferences.commit();

      try {
        MainActivity activity = (MainActivity) context;
        activity.updateLastExportedDate();
      } catch (Exception e) {
        Log.d("TodoDatabaseExport", "Failed to update last export date on main activity");
      }

      Toast.makeText(context, R.string.toast_export_success, Toast.LENGTH_LONG).show();
    } else {
      Toast.makeText(context.getApplicationContext(), R.string.toast_export_failed, Toast.LENGTH_SHORT).show();
    }
  }

  /**
   * Retrieves the {@link Todo}s from the database.
   *
   * @return The list of {@link Todo}s.
   */
  private List<Todo> queryAllTodos() {
    TodoOpenHelper openHelper = new TodoOpenHelper(context);
    SQLiteDatabase db = openHelper.getReadableDatabase();
    Cursor cursor = db.rawQuery(SELECT_ALL_TODOS, new String[]{});

    List<Todo> todos = new ArrayList<>();
    while (cursor.moveToNext()) {
      todos.add(TodoDatabaseUtils.extractTodoFromCursor(cursor));
    }

    cursor.close();
    db.close();
    openHelper.close();

    return todos;
  }

  /**
   * Writes the {@link Todo} list to a json file.
   *
   * @param out   The {@link OutputStream} for the file.
   * @param todos The list of {@link Todo}s to export.
   * @throws IOException if any error occurs during writing to the file.
   */
  private void writeTodoList(OutputStream out, List<Todo> todos) throws IOException {
    JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
    writer.beginArray();
    for (Todo todo : todos) {
      writeTodo(writer, todo);
    }
    writer.endArray();
    writer.close();
  }

  /**
   * Writes a given {@link Todo} to a json file using the given {@link JsonWriter}.
   *
   * @param writer The {@link JsonWriter} to use.
   * @param todo   The {@link Todo} to write.
   * @throws IOException if any error occurs during writing to the file.
   */
  private void writeTodo(JsonWriter writer, Todo todo) throws IOException {
    writer.beginObject();
    writer.name("id").value(todo.getId());
    writer.name("title").value(todo.getTitle());
    writer.name("category").value(todo.getCategory().toString());
    writer.name("color").value(todo.getColor().toString());
    writer.name("datetime").value(JSON_DATE_FORMAT.format(todo.getDateTime().getTime()));
    writer.name("summary").value(todo.getSummary());
    writer.endObject();
  }

}
