package hu.szte.baavagt.todoreminder;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.Callable;

import hu.szte.baavagt.todoreminder.database.TodoOpenHelper;
import hu.szte.baavagt.todoreminder.dialog.CategoryPickerDialogFragment;
import hu.szte.baavagt.todoreminder.dialog.ColorPickerDialogFragment;
import hu.szte.baavagt.todoreminder.listener.DateChangeListener;
import hu.szte.baavagt.todoreminder.listener.TimeChangeListener;
import hu.szte.baavagt.todoreminder.model.Todo;
import hu.szte.baavagt.todoreminder.model.enums.TodoCategory;
import hu.szte.baavagt.todoreminder.model.enums.TodoColor;
import hu.szte.baavagt.todoreminder.utils.TodoDatabaseUtils;

import static hu.szte.baavagt.todoreminder.database.TodoReminderContract.TodoEntry;

/**
 * Activity class for creating new {@link Todo}s.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class NewTodoActivity extends AppCompatActivity {

  private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy. MMM dd. HH:mm");

  private static final String PREF_KEY = "hu.szte.baavagt.todoreminder";
  private static final String PREF_LAST_CATEGORY_KEY = "lastSelectedCategory";
  private static final String PREF_LAST_COLOR_KEY = "lastSelectedColor";

  private Todo todo;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_new_todo);

    Object intentTodo = getIntent().getSerializableExtra("todo");
    if (intentTodo != null) {
      todo = (Todo) intentTodo;

      EditText title = (EditText) findViewById(R.id.new_todo_title);
      title.setText(todo.getTitle());

      EditText summary = (EditText) findViewById(R.id.new_todo_summary);
      summary.setText(todo.getSummary());
    } else {
      todo = new Todo();

      SharedPreferences preferences = getSharedPreferences(PREF_KEY, MODE_PRIVATE);
      String lastCategory = preferences.getString(PREF_LAST_CATEGORY_KEY, "NONE");
      String lastColor = preferences.getString(PREF_LAST_COLOR_KEY, "GRAY");

      todo.setCategory(TodoCategory.valueOf(lastCategory));
      todo.setColor(TodoColor.valueOf(lastColor));
    }

    // Enables the 'Add' button if the title isn't empty
    EditText title = (EditText) findViewById(R.id.new_todo_title);
    title.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {
        EditText editText = (EditText) v;
        if (!hasFocus) {
          Button addTodoButton = (Button) findViewById(R.id.new_todo_add_button);
          addTodoButton.setEnabled(editText.getText() != null && editText.getText().length() >= 1);
        }
      }
    });
  }

  /**
   * Updates the form fields from the {@link Todo} object when returning from the picker fragments.
   */
  @Override
  protected void onResume() {
    super.onResume();

    try {
      categoryUpdater.call();
      colorUpdater.call();
      dateTimeUpdater.call();
    } catch (Exception e) {
      Log.e("NewTodoActivity", "Failed to update form fields!", e);
    }
  }

  /**
   * Adds the {@link Todo} to the database.
   *
   * @param view The button.
   */
  public void addTodo(View view) {
    extractTodoValuesFromFields();

    ContentValues values = TodoDatabaseUtils.createValuesFromTodo(todo);

    SQLiteDatabase db = new TodoOpenHelper(this).getWritableDatabase();
    if (todo.getId() != null) {
      if (db.update(TodoEntry.TABLE_NAME, values, TodoEntry._ID + "=?", new String[]{String.valueOf(todo.getId())}) != -1) {
        SharedPreferences.Editor preferences = getSharedPreferences(PREF_KEY, MODE_PRIVATE).edit();
        preferences.putString(PREF_LAST_CATEGORY_KEY, todo.getCategory().toString());
        preferences.putString(PREF_LAST_COLOR_KEY, todo.getColor().toString());
        preferences.apply();

        Toast.makeText(this, R.string.toast_todo_added, Toast.LENGTH_SHORT).show();
        finish();
      }
    } else {
      if (db.insert(TodoEntry.TABLE_NAME, null, values) != -1) {
        SharedPreferences.Editor preferences = getSharedPreferences(PREF_KEY, MODE_PRIVATE).edit();
        preferences.putString(PREF_LAST_CATEGORY_KEY, todo.getCategory().toString());
        preferences.putString(PREF_LAST_COLOR_KEY, todo.getColor().toString());
        preferences.apply();

        Toast.makeText(this, R.string.toast_todo_added, Toast.LENGTH_SHORT).show();
        finish();
      }
    }
  }

  /**
   * Discards the form, returning to the {@link MainActivity} without saving.
   *
   * @param view The button.
   */
  public void discardTodo(View view) {
    finish();
  }

  /**
   * Fills the {@link Todo} with the data from the input fields.
   */
  private void extractTodoValuesFromFields() {
    EditText title = (EditText) findViewById(R.id.new_todo_title);
    todo.setTitle(title.getText().toString());

    EditText summary = (EditText) findViewById(R.id.new_todo_summary);
    todo.setSummary(summary.getText().toString());
  }

  /**
   * Opens the category picker dialog.
   *
   * @param view The button.
   */
  public void openCategoryPicker(View view) {
    CategoryPickerDialogFragment dialog = new CategoryPickerDialogFragment();
    dialog.setTodo(todo);
    dialog.setCallback(categoryUpdater);
    dialog.show(getFragmentManager(), "CategoryPickerDialogFragment");
  }

  /**
   * Opens the color picker dialog.
   *
   * @param view The button.
   */
  public void openColorPicker(View view) {
    ColorPickerDialogFragment dialog = new ColorPickerDialogFragment();
    dialog.setTodo(todo);
    dialog.setCallback(colorUpdater);
    dialog.show(getFragmentManager(), "ColorPickerDialogFragment");
  }

  /**
   * Opens the date picker dialog.
   *
   * @param view The button.
   */
  public void openDatePicker(View view) {
    Calendar date = todo.getDateTime();
    int year = date.get(Calendar.YEAR);
    int month = date.get(Calendar.MONTH);
    int day = date.get(Calendar.DAY_OF_MONTH);

    DatePickerDialog dialog = new DatePickerDialog(this, new DateChangeListener(todo, dateTimeUpdater), year, month, day);
    dialog.show();
  }

  /**
   * Opens the time picker dialog.
   *
   * @param view The button.
   */
  public void openTimePicker(View view) {
    Calendar time = todo.getDateTime();
    int hour = time.get(Calendar.HOUR_OF_DAY);
    int minute = time.get(Calendar.MINUTE);

    TimePickerDialog dialog = new TimePickerDialog(this, new TimeChangeListener(todo, dateTimeUpdater), hour, minute, true);

    dialog.show();
  }

  /**
   * A callback for updating the category display from the {@link Todo} when the user picked a category from the dialog.
   */
  private Callable<Void> categoryUpdater = new Callable<Void>() {

    @Override
    public Void call() throws Exception {
      TextView categoryLabel = (TextView) findViewById(R.id.new_todo_category_label);
      categoryLabel.setText(todo.getCategory().getLabel());

      ImageView icon = (ImageView) findViewById(R.id.new_todo_category_icon);
      icon.setBackgroundResource(todo.getCategory().getDrawable());

      return null;
    }

  };

  /**
   * A callback for updating the color display from the {@link Todo} when the user picked a color from the dialog.
   */
  private Callable<Void> colorUpdater = new Callable<Void>() {

    @Override
    public Void call() throws Exception {
      TextView colorLabel = (TextView) findViewById(R.id.new_todo_color_label);
      colorLabel.setText(todo.getColor().getLabel());

      ImageView icon = (ImageView) findViewById(R.id.new_todo_color_icon);
      icon.setBackgroundResource(todo.getColor().getDrawable());

      return null;
    }

  };

  /**
   * A callback for updating the dateTime input field from the {@link Todo} when the user picked a date or a time.
   */
  private Callable<Void> dateTimeUpdater = new Callable<Void>() {

    @Override
    public Void call() throws Exception {
      EditText dateTimeDisplay = (EditText) findViewById(R.id.new_todo_datetime);
      dateTimeDisplay.setText(DATE_TIME_FORMAT.format(todo.getDateTime().getTime()));

      return null;
    }

  };

}
