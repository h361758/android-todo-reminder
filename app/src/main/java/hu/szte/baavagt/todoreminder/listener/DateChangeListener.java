package hu.szte.baavagt.todoreminder.listener;

import android.app.DatePickerDialog;
import android.util.Log;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.concurrent.Callable;

import hu.szte.baavagt.todoreminder.model.Todo;

/**
 * Listener for the {@link DatePickerDialog}.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class DateChangeListener implements DatePickerDialog.OnDateSetListener {

  private Todo todo;
  private Callable<Void> callback;

  public DateChangeListener(Todo todo, Callable<Void> callback) {
    this.todo = todo;
    this.callback = callback;
  }

  /**
   * Updates the dateTime field in the {@link Todo} object with the date selected.
   *
   * @param view       The date picker.
   * @param year       The year selected.
   * @param month      The month selected.
   * @param dayOfMonth THe day selected.
   */
  @Override
  public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
    Calendar dateTime = todo.getDateTime();
    dateTime.set(Calendar.YEAR, year);
    dateTime.set(Calendar.MONTH, month);
    dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);

    if (callback != null) {
      try {
        callback.call();
      } catch (Exception e) {
        Log.e("DateChangeListener", "Failed to call callback!", e);
      }
    }
  }

}
