package hu.szte.baavagt.todoreminder.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import java.util.concurrent.Callable;

import hu.szte.baavagt.todoreminder.R;
import hu.szte.baavagt.todoreminder.listener.ColorChangeOnClickListener;
import hu.szte.baavagt.todoreminder.model.Todo;
import hu.szte.baavagt.todoreminder.model.enums.TodoColor;

/**
 * Dialog fragment for picking a color for the {@link Todo}.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class ColorPickerDialogFragment extends DialogFragment {

  private Todo todo;
  private Callable<Void> callback;

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = getActivity().getLayoutInflater();

    View view = inflater.inflate(R.layout.fragment_color_picker_dialog, null);
    addColorPickListeners(view);

    builder.setView(view);
    return builder.create();
  }

  /**
   * Adds the onClick listeners for each color.
   *
   * @param view The parent view for the colors.
   */
  private void addColorPickListeners(View view) {
    view.findViewById(R.id.color_picker_black).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.BLACK, this, callback));
    view.findViewById(R.id.color_picker_darkgray).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.DARK_GRAY, this, callback));
    view.findViewById(R.id.color_picker_gray).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.GRAY, this, callback));
    view.findViewById(R.id.color_picker_blue).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.BLUE, this, callback));
    view.findViewById(R.id.color_picker_green).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.GREEN, this, callback));
    view.findViewById(R.id.color_picker_purple).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.PURPLE, this, callback));
    view.findViewById(R.id.color_picker_yellow).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.YELLOW, this, callback));
    view.findViewById(R.id.color_picker_orange).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.ORANGE, this, callback));
    view.findViewById(R.id.color_picker_red).setOnClickListener(
      new ColorChangeOnClickListener(todo, TodoColor.RED, this, callback));
  }

  public void setTodo(Todo todo) {
    this.todo = todo;
  }

  public void setCallback(Callable<Void> callback) {
    this.callback = callback;
  }

}
