package hu.szte.baavagt.todoreminder.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import java.util.concurrent.Callable;

import hu.szte.baavagt.todoreminder.R;
import hu.szte.baavagt.todoreminder.listener.CategoryChangeOnClickListener;
import hu.szte.baavagt.todoreminder.model.Todo;
import hu.szte.baavagt.todoreminder.model.enums.TodoCategory;

/**
 * Dialog fragment for picking a category.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class CategoryPickerDialogFragment extends DialogFragment {

  private Todo todo;
  private Callable<Void> callback;

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = getActivity().getLayoutInflater();

    View view = inflater.inflate(R.layout.fragment_category_picker_dialog, null);
    addColorPickListeners(view);

    builder.setView(view);
    return builder.create();
  }

  /**
   * Adds the onClick listeners for each category.
   *
   * @param view The parent view for the categories.
   */
  private void addColorPickListeners(View view) {
    view.findViewById(R.id.category_picker_none).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.NONE, this, callback));
    view.findViewById(R.id.category_picker_anniversary).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.ANNIVERSARY, this, callback));
    view.findViewById(R.id.category_picker_shopping).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.SHOPPING, this, callback));
    view.findViewById(R.id.category_picker_call).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.CALL, this, callback));
    view.findViewById(R.id.category_picker_meeting).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.MEETING, this, callback));
    view.findViewById(R.id.category_picker_school).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.SCHOOL, this, callback));
    view.findViewById(R.id.category_picker_sport).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.SPORT, this, callback));
    view.findViewById(R.id.category_picker_date).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.DATE, this, callback));
    view.findViewById(R.id.category_picker_travel).setOnClickListener(
      new CategoryChangeOnClickListener(todo, TodoCategory.TRAVEL, this, callback));
  }

  public void setTodo(Todo todo) {
    this.todo = todo;
  }

  public void setCallback(Callable<Void> callback) {
    this.callback = callback;
  }

}
