package hu.szte.baavagt.todoreminder;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import hu.szte.baavagt.todoreminder.database.TodoOpenHelper;
import hu.szte.baavagt.todoreminder.model.Todo;

import static hu.szte.baavagt.todoreminder.database.TodoReminderContract.TodoEntry;

/**
 * Simple Todo information activity. Displays the data in the {@link Todo}.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class TodoInfoActivity extends AppCompatActivity {

  private static final SimpleDateFormat TODO_INFO_DATETIME_FORMAT = new SimpleDateFormat("yyyy. MM. dd. HH:mm");

  private Todo todo;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_todo_info);

    todo = (Todo) getIntent().getSerializableExtra("todo");
    updateFields();

    startAnimations();
  }

  @Override
  protected void onResume() {
    super.onResume();
    updateFields();
  }

  public void editTodo(View view) {
    Intent intent = new Intent(this, NewTodoActivity.class);
    intent.putExtra("todo", todo);
    startActivity(intent);
  }

  public void deleteTodo(View view) {
    new AlertDialog.Builder(this)
      .setTitle(R.string.todo_info_delete)
      .setMessage(R.string.todo_info_delete_confirm)
      .setIcon(android.R.drawable.ic_dialog_alert)
      .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int whichButton) {
          performTodoDelete();
        }
      })
      .setNegativeButton(android.R.string.no, null).show();
  }

  private void performTodoDelete() {
    SQLiteDatabase db = new TodoOpenHelper(this).getWritableDatabase();
    db.delete(TodoEntry.TABLE_NAME, TodoEntry._ID + "=?", new String[]{String.valueOf(todo.getId())});

    Toast.makeText(this, R.string.todo_info_delete_done, Toast.LENGTH_SHORT).show();
    finish();
  }

  /**
   * Starts the fade-in animations for the fields.
   */
  private void startAnimations() {
    TextView title = (TextView) findViewById(R.id.todo_info_title);
    TextView dateTime = (TextView) findViewById(R.id.todo_info_datetime);
    ImageView colorBar = (ImageView) findViewById(R.id.todo_info_color_bar);
    TextView summary = (TextView) findViewById(R.id.todo_info_summary);

    ObjectAnimator titleAlphaAnimator = ObjectAnimator.ofFloat(title, "alpha", 0f, 1f).setDuration(500);
    titleAlphaAnimator.setStartDelay(200);
    titleAlphaAnimator.start();

    ObjectAnimator titleTranslationXAnimator = ObjectAnimator.ofFloat(title, "translationX", -24f, 0f).setDuration(500);
    titleTranslationXAnimator.setStartDelay(200);
    titleTranslationXAnimator.start();

    ObjectAnimator dateTimeAlphaAnimator = ObjectAnimator.ofFloat(dateTime, "alpha", 0f, 1f).setDuration(500);
    dateTimeAlphaAnimator.setStartDelay(200);
    dateTimeAlphaAnimator.start();

    ObjectAnimator dateTimeTranslationXAnimator = ObjectAnimator.ofFloat(dateTime, "translationX", -24f, 0f).setDuration(500);
    dateTimeTranslationXAnimator.setStartDelay(200);
    dateTimeTranslationXAnimator.start();

    ObjectAnimator summaryAlphaAnimator = ObjectAnimator.ofFloat(summary, "alpha", 0f, 1f).setDuration(500);
    summaryAlphaAnimator.setStartDelay(200);
    summaryAlphaAnimator.start();

    ObjectAnimator summaryTranslationXAnimator = ObjectAnimator.ofFloat(summary, "translationX", -24f, 0f).setDuration(500);
    summaryTranslationXAnimator.setStartDelay(200);
    summaryTranslationXAnimator.start();

    ObjectAnimator colorBarAnimator = ObjectAnimator.ofFloat(colorBar, "scaleX", 0f, 1f).setDuration(500);
    colorBarAnimator.setStartDelay(700);
    colorBarAnimator.setInterpolator(new FastOutSlowInInterpolator());
    colorBarAnimator.start();
  }

  /**
   * Fills the textViews and icons with the data from the {@link Todo} object.
   */
  private void updateFields() {
    ImageView categoryIcon = (ImageView) findViewById(R.id.todo_info_category_icon);
    categoryIcon.setImageResource(todo.getCategory().getDrawable());

    TextView title = (TextView) findViewById(R.id.todo_info_title);
    title.setText(todo.getTitle());

    TextView dateTime = (TextView) findViewById(R.id.todo_info_datetime);
    dateTime.setText(TODO_INFO_DATETIME_FORMAT.format(todo.getDateTime().getTime()));

    ImageView colorBar = (ImageView) findViewById(R.id.todo_info_color_bar);
    colorBar.setImageResource(todo.getColor().getDrawable());

    TextView summary = (TextView) findViewById(R.id.todo_info_summary);
    summary.setText(todo.getSummary());
  }

}
