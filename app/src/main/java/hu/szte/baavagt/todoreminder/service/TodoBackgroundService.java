package hu.szte.baavagt.todoreminder.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import hu.szte.baavagt.todoreminder.utils.TodoBackgroundThread;

/**
 * The background service for the application. It runs indefinitely and checks for the
 * {@link hu.szte.baavagt.todoreminder.model.Todo}s that need a notification.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class TodoBackgroundService extends IntentService {

  private TodoBackgroundThread todoBackgroundThread;

  public TodoBackgroundService() {
    super("Todo Reminder background service");
  }

  @Override
  public void onStart(@Nullable Intent intent, int startId) {
    super.onStart(intent, startId);

    // Starts the background thread if it hasn't been already
    if (todoBackgroundThread == null) {
      todoBackgroundThread = new TodoBackgroundThread(this);
      todoBackgroundThread.start();
    }
  }

  @Override
  public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
    if (todoBackgroundThread == null) {
      todoBackgroundThread = new TodoBackgroundThread(this);
      todoBackgroundThread.start();
    }

    // Starts the background service in sticky mode
    return START_STICKY;
  }

  @Override
  protected void onHandleIntent(@Nullable Intent intent) {
    // No intents expected
  }

  @Override
  public void onDestroy() {
    super.onDestroy();

    // Stops the background thread
    todoBackgroundThread.halt();
  }

}
