package hu.szte.baavagt.todoreminder.listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import hu.szte.baavagt.todoreminder.TodoInfoActivity;
import hu.szte.baavagt.todoreminder.model.Todo;

/**
 * OnClick listener for the {@link Todo} list {@link ListView}.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class TodoListItemOnClickListener implements ListView.OnItemClickListener {

  private Context context;

  public TodoListItemOnClickListener(Context context) {
    this.context = context;
  }

  /**
   * Opens the {@link TodoInfoActivity} for the clicked {@link Todo} item.
   *
   * @param parent   The {@link Todo} adapter view.
   * @param view     The actual view.
   * @param position The position in the {@link Todo} array.
   * @param id       The row id.
   */
  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Todo todo = (Todo) parent.getItemAtPosition(position);

    Intent intent = new Intent(context, TodoInfoActivity.class);
    intent.putExtra("todo", todo);
    context.startActivity(intent);
  }

}
