package hu.szte.baavagt.todoreminder;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import hu.szte.baavagt.todoreminder.adapter.TodoAdapter;
import hu.szte.baavagt.todoreminder.database.TodoOpenHelper;
import hu.szte.baavagt.todoreminder.listener.TodoListItemOnClickListener;
import hu.szte.baavagt.todoreminder.model.Todo;
import hu.szte.baavagt.todoreminder.service.TodoBackgroundService;
import hu.szte.baavagt.todoreminder.utils.DatabaseExportAsyncTask;
import hu.szte.baavagt.todoreminder.utils.TodoDatabaseUtils;

/**
 * The main activity of the application. Lists the todos.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class MainActivity extends AppCompatActivity {

  private static final SimpleDateFormat JSON_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.UK);

  private static final String PREF_KEY = "hu.szte.baavagt.todoreminder";
  private static final String PREF_LAST_EXPORT = "lastExported";

  private static final String SELECT_ALL_TODOS = "SELECT * FROM todo;";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    Intent intent = new Intent(this, TodoBackgroundService.class);
    startService(intent);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        openNewTodoActivity();
      }
    });

    startOpeningAnimation();
  }

  /**
   * Refreshed the {@link Todo} {@link ListView}.
   */
  @Override
  protected void onResume() {
    super.onResume();

    List<Todo> todos = queryTodos();
    ListView listView = (ListView) findViewById(R.id.todo_list_view);
    listView.setAdapter(new TodoAdapter(this, R.layout.list_todo_item, todos.toArray(new Todo[]{})));
    listView.setOnItemClickListener(new TodoListItemOnClickListener(this));
  }

  /**
   * Starts the opening splash-screen animation.
   */
  private void startOpeningAnimation() {
    LinearLayout splashScreen = (LinearLayout) findViewById(R.id.splash_screen);
    ObjectAnimator splashScreenAnimator = ObjectAnimator.ofFloat(splashScreen, "alpha", 0f);
    splashScreenAnimator.setStartDelay(1500);
    splashScreenAnimator.setDuration(500);
    splashScreenAnimator.start();

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    ObjectAnimator fabAnimator = ObjectAnimator.ofFloat(fab, "alpha", 0f, 1f);
    fabAnimator.setStartDelay(1500);
    fabAnimator.setDuration(500);
    fabAnimator.start();
  }

  /**
   * Opens the {@link NewTodoActivity}.
   */
  private void openNewTodoActivity() {
    Intent intent = new Intent(this, NewTodoActivity.class);
    startActivity(intent);
  }

  /**
   * Queries all {@link Todo}s from the database.
   *
   * @return The list of {@link Todo}s in the database.
   */
  private List<Todo> queryTodos() {
    TodoOpenHelper dbOpenHelper = new TodoOpenHelper(this);
    SQLiteDatabase db = dbOpenHelper.getReadableDatabase();

    List<Todo> todos = new ArrayList<Todo>();
    Cursor cursor = db.rawQuery(SELECT_ALL_TODOS, new String[]{});
    while (cursor.moveToNext()) {
      todos.add(TodoDatabaseUtils.extractTodoFromCursor(cursor));
    }

    return todos;
  }

  public void updateLastExportedDate() {
    SharedPreferences preferences = getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
    String lastExportedDate = preferences.getString(PREF_LAST_EXPORT, "");

    TextView lastExportedDateLabel = (TextView) findViewById(R.id.last_exported_date_label);
    lastExportedDateLabel.setText(lastExportedDate.replace(" ", "\n"));
  }

  /**
   * Starts a database export task. Requests permission for writing external storage if needed.
   *
   * @param view The clicked button.
   */
  public void exportDatabase(View view) {
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    } else {
      new DatabaseExportAsyncTask().execute(this);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    switch (requestCode) {
      case 1:
        if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          new DatabaseExportAsyncTask().execute(this);
        } else {
          Toast.makeText(this, R.string.toast_export_permission_error, Toast.LENGTH_LONG).show();
        }
        return;
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

}
