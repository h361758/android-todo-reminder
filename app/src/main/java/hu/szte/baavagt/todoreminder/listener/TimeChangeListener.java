package hu.szte.baavagt.todoreminder.listener;

import android.app.TimePickerDialog;
import android.util.Log;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.concurrent.Callable;

import hu.szte.baavagt.todoreminder.model.Todo;

/**
 * Listener for the {@link TimePickerDialog}.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class TimeChangeListener implements TimePickerDialog.OnTimeSetListener {

  private Todo todo;
  private Callable<Void> callback;

  public TimeChangeListener(Todo todo, Callable<Void> callback) {
    this.todo = todo;
    this.callback = callback;
  }

  /**
   * Updates the dateTime field in the {@link Todo} object with the time selected.
   *
   * @param view      The time picker.
   * @param hourOfDay The hour selected.
   * @param minute    The minute selected.
   */
  @Override
  public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
    Calendar dateTime = todo.getDateTime();
    dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
    dateTime.set(Calendar.MINUTE, minute);

    if (callback != null) {
      try {
        callback.call();
      } catch (Exception e) {
        Log.e("TimeChangeListener", "Failed to call callback!", e);
      }
    }
  }

}
