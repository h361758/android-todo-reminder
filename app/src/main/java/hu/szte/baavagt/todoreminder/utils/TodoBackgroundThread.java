package hu.szte.baavagt.todoreminder.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import hu.szte.baavagt.todoreminder.database.TodoOpenHelper;
import hu.szte.baavagt.todoreminder.model.Todo;

/**
 * The background thread for the application. It decides when to throw a notification for the {@link Todo}s.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class TodoBackgroundThread extends Thread {

  private static final int TICK_INTERVAL = 30000;
  private static final String SELECT_ALL_TODOS_WITHIN_FIVE_MINUTES =
    "SELECT * FROM todo WHERE DATETIME(todo.date_time) BETWEEN DATETIME('now') AND (DATETIME('now', '+5 minutes'));";

  private boolean haltRequested = false;

  private Context context;
  private SQLiteDatabase db;
  private TodoOpenHelper dbOpenHelper;

  public TodoBackgroundThread(Context context) {
    this.context = context;

    dbOpenHelper = new TodoOpenHelper(context);
    db = dbOpenHelper.getReadableDatabase();
  }

  @Override
  public void run() {
    while (!haltRequested) {
      Cursor cursor = db.rawQuery(SELECT_ALL_TODOS_WITHIN_FIVE_MINUTES, new String[]{});
      while (cursor.moveToNext()) {
        Todo todo = TodoDatabaseUtils.extractTodoFromCursor(cursor);
        NotificationUtils.throwNotificationForTodo(context, todo);
      }

      cursor.close();

      // Sleep the thread for the TICK_INTERVAL so it doesn't use unnecessary resources.
      try {
        Thread.sleep(TICK_INTERVAL);
      } catch (InterruptedException e) {
        // No need to handle this, a halt() is requested, should stop.
      }
    }

    dbOpenHelper.close();
    db.close();
  }

  /**
   * Notifies the background thread to stop.
   */
  public void halt() {
    this.haltRequested = true;
    interrupt();
  }

}