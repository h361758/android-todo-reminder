package hu.szte.baavagt.todoreminder.listener;

import android.app.DialogFragment;
import android.util.Log;
import android.view.View;

import java.util.concurrent.Callable;

import hu.szte.baavagt.todoreminder.model.Todo;
import hu.szte.baavagt.todoreminder.model.enums.TodoCategory;

/**
 * OnClick listener for the {@link hu.szte.baavagt.todoreminder.dialog.CategoryPickerDialogFragment}.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class CategoryChangeOnClickListener implements View.OnClickListener {

  private Todo todo;
  private TodoCategory category;
  private DialogFragment dialog;
  private Callable<Void> callback;

  public CategoryChangeOnClickListener(Todo todo, TodoCategory category, DialogFragment dialog, Callable<Void> callback) {
    this.todo = todo;
    this.category = category;
    this.dialog = dialog;
    this.callback = callback;
  }

  /**
   * When a category is picked, the {@link Todo} object is updated and the dialog is dismissed. If a callback was
   * specified, it is also called.
   *
   * @param view The category icon that was clicked on.
   */
  @Override
  public void onClick(View view) {
    todo.setCategory(category);

    if (callback != null) {
      try {
        callback.call();
      } catch (Exception e) {
        Log.e("ColorChangeListener", "Failed to call color change callback!", e);
      }
    }

    dialog.dismiss();
  }

}
