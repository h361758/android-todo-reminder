package hu.szte.baavagt.todoreminder.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static hu.szte.baavagt.todoreminder.database.TodoReminderContract.TodoEntry;

/**
 * {@link SQLiteOpenHelper} subclass for {@link hu.szte.baavagt.todoreminder.model.Todo}s.
 */
public class TodoOpenHelper extends SQLiteOpenHelper {

  private static final int DATABASE_VERSION = 5;
  private static final String DATABASE_NAME = "todoreminder.db";

  private static final String CREATE_TODO_TABLE =
    "CREATE TABLE " + TodoEntry.TABLE_NAME + " (" +
      TodoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
      TodoEntry.COLUMN_TITLE + " TEXT, " +
      TodoEntry.COLUMN_CATEGORY + " TEXT, " +
      TodoEntry.COLUMN_COLOR + " TEXT, " +
      TodoEntry.COLUMN_DATE_TIME + " TEXT, " +
      TodoEntry.COLUMN_SUMMARY + " TEXT" +
      ")";
  private static final String DROP_TODO_TABLE = "DROP TABLE " + TodoEntry.TABLE_NAME + ";";

  public TodoOpenHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_TODO_TABLE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // Simply drops the previous tables and recreates them.
    db.execSQL(DROP_TODO_TABLE);

    onCreate(db);
  }

}
