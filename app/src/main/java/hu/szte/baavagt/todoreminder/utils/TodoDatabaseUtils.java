package hu.szte.baavagt.todoreminder.utils;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import hu.szte.baavagt.todoreminder.model.Todo;
import hu.szte.baavagt.todoreminder.model.enums.TodoCategory;
import hu.szte.baavagt.todoreminder.model.enums.TodoColor;

import static hu.szte.baavagt.todoreminder.database.TodoReminderContract.TodoEntry;

/**
 * Utility class for database-related functions, data type conversions, etc.
 *
 * @author Adam Bankeszi {@literal BAAVAGT.SZE}
 */
public class TodoDatabaseUtils {

  /**
   * Date format for storing and retrieving {@link Calendar}s to and from the database.
   */
  private static final SimpleDateFormat DATABASE_CALENDAR_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

  /**
   * Instantiation intentionally disabled to indicate that it should only be accessed in a static way.
   */
  private TodoDatabaseUtils() {

  }

  /**
   * Creates a {@link Todo} from the current position of the cursor.
   *
   * @param cursor The {@link Cursor} to read data from.
   * @return The {@link Todo} object extracted from the cursor.
   */
  public static Todo extractTodoFromCursor(Cursor cursor) {
    int id = cursor.getInt(cursor.getColumnIndex(TodoEntry._ID));
    String title = cursor.getString(cursor.getColumnIndex(TodoEntry.COLUMN_TITLE));
    String datetime = cursor.getString(cursor.getColumnIndex(TodoEntry.COLUMN_DATE_TIME));
    TodoCategory category = TodoCategory.valueOf(cursor.getString(cursor.getColumnIndex(TodoEntry.COLUMN_CATEGORY)));
    TodoColor color = TodoColor.valueOf(cursor.getString(cursor.getColumnIndex(TodoEntry.COLUMN_COLOR)));
    String summary = cursor.getString(cursor.getColumnIndex(TodoEntry.COLUMN_SUMMARY));

    Todo todo = new Todo();
    todo.setId(id);
    todo.setTitle(title);
    todo.setCategory(category);
    todo.setColor(color);
    todo.setDateTime(convertStringToCalendar(datetime));
    todo.setSummary(summary);
    return todo;
  }

  /**
   * Creates a {@link ContentValues} object from a {@link Todo} for insert and update queries.
   *
   * @param todo The {@link Todo} object to use.
   * @return The result {@link ContentValues}.
   */
  public static ContentValues createValuesFromTodo(Todo todo) {
    ContentValues values = new ContentValues();

    if (todo.getId() != null) {
      values.put(TodoEntry._ID, todo.getId());
    }

    values.put(TodoEntry.COLUMN_TITLE, todo.getTitle());
    values.put(TodoEntry.COLUMN_CATEGORY, todo.getCategory().toString());
    values.put(TodoEntry.COLUMN_COLOR, todo.getColor().toString());
    values.put(TodoEntry.COLUMN_DATE_TIME, convertCalendarToString(todo.getDateTime()));
    values.put(TodoEntry.COLUMN_SUMMARY, todo.getSummary());

    return values;
  }

  /**
   * Converts a {@link Calendar} object to a {@link String} for database queries.
   *
   * @param calendar The {@link Calendar} to convert.
   * @return The result formatted {@link String}.
   */
  public static String convertCalendarToString(Calendar calendar) {
    return DATABASE_CALENDAR_FORMAT.format(calendar.getTime());
  }

  /**
   * Converts a properly formatted {@link String} to a {@link Calendar}. The calendarString should be in a format
   * defined by {@link TodoDatabaseUtils#DATABASE_CALENDAR_FORMAT}. If the calendarString is not well formatted, it
   * will return a {@link Calendar} instance for the current date and time.
   *
   * @param calendarString The {@link String} to convert.
   * @return The result Calendar object.
   */
  public static Calendar convertStringToCalendar(String calendarString) {
    Calendar calendar = Calendar.getInstance();

    try {
      calendar.setTime(DATABASE_CALENDAR_FORMAT.parse(calendarString));
    } catch (ParseException e) {
      Log.e("TodoDatabaseUtils",
        "Failed to parse date from: '" + calendarString + "'. Returning default current time instead.");
    }

    return calendar;
  }

}
