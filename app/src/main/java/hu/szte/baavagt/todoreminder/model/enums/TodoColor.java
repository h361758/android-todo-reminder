package hu.szte.baavagt.todoreminder.model.enums;

import hu.szte.baavagt.todoreminder.R;

/**
 * Specifies the colors for the {@link hu.szte.baavagt.todoreminder.model.Todo}s. Also holds references for the
 * labels and the drawable colors for convenience.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public enum TodoColor {

  BLACK(R.string.color_black, R.color.todoBlack),
  DARK_GRAY(R.string.color_dark_gray, R.color.todoDarkGray),
  GRAY(R.string.color_gray, R.color.todoGray),
  BLUE(R.string.color_blue, R.color.todoBlue),
  GREEN(R.string.color_green, R.color.todoGreen),
  PURPLE(R.string.color_purple, R.color.todoPurple),
  YELLOW(R.string.color_yellow, R.color.todoYellow),
  ORANGE(R.string.color_orange, R.color.todoOrange),
  RED(R.string.color_red, R.color.todoRed);

  private int label;
  private int drawable;

  TodoColor(int label, int drawable) {
    this.label = label;
    this.drawable = drawable;
  }

  public int getLabel() {
    return label;
  }

  public int getDrawable() {
    return drawable;
  }

}
