package hu.szte.baavagt.todoreminder.model;

import java.io.Serializable;
import java.util.Calendar;

import hu.szte.baavagt.todoreminder.model.enums.TodoCategory;
import hu.szte.baavagt.todoreminder.model.enums.TodoColor;

/**
 * Stores information of a todo.
 *
 * @author Adam Bankeszi {@literal <BAAVAGT.SZE>}
 */
public class Todo implements Serializable {

  private Integer id;

  private String title;
  private String summary;
  private Calendar dateTime;
  private TodoCategory category;
  private TodoColor color;

  public Todo() {
    dateTime = Calendar.getInstance();
    category = TodoCategory.NONE;
    color = TodoColor.GRAY;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public Calendar getDateTime() {
    return dateTime;
  }

  public void setDateTime(Calendar dateTime) {
    this.dateTime = dateTime;
  }

  public TodoCategory getCategory() {
    return category;
  }

  public void setCategory(TodoCategory category) {
    this.category = category;
  }

  public TodoColor getColor() {
    return color;
  }

  public void setColor(TodoColor color) {
    this.color = color;
  }

  @Override
  public String toString() {
    return "Todo{" +
      "title='" + title + '\'' +
      ", summary='" + summary + '\'' +
      ", dateTime=" + dateTime +
      ", category=" + category +
      '}';
  }

}
