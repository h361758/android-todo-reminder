# Todo Reminder #

A simple Android application for handling todos.

### Features ###
* Create simple Todos
    * Add title, summary, category and color
    * Remembers your last category and color selection
* Edit or delete your todos.

* Simple splash-screen animation
* Background service checks your notifications every 30 seconds
* 5-minute reminder notifications
    * Plays a sound effect for the notifications
* Export your database in a JSON format

### Compatibility ###
* Minimum Android version: 4.1

### Created by ###

* Ádám Bánkeszi <BAAVAGT.SZE>